<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'dbMysql' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=csgestor_admin;host=127.0.0.1;port=3306',
        'username' => 'csgestor_max',
        'password' => 'amdsdl7586',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ],
    'service_manager' => array(
        'aliases' => [
            'adapter' => 'dbMysql',
        ],
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'dbMysql' => function($sm) {
                $config = $sm->get('config');
                $config = $config['dbMysql'];
                $dbAdapter = new Zend\Db\Adapter\Adapter($config);
                return $dbAdapter;
            },
        ),
    ),
);
