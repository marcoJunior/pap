var clientes = new function () {
    $('[name=radio2]').on('click', function () {
        var tipo = $('[name=radio2]:checked').val();
        var esconde = tipo === 'J' ? 'F' : 'J';
        $(`.P${tipo}`).show();
        $(`.P${esconde}`).hide();
    });
};

$(document).ready(function () {

    $("#cadastroFornecedores").validate({
        rules: {
            //Login
            emailLogin: {
                required: true,
            },
            senhaLogin: {
                required: true,
            },
            //Pessoas Fisica
            nomeCompleto: {
                required: true,
            },
            cpf: {
                required: true,
            },
            dataNascimento: {
                required: true,
            },
            radio3: {
                required: true,
            },
            //Pessoas Juridica
            razaoSocial: {
                required: true,
            },
            fantasia: {
                required: true,
            },
            cnpj: {
                required: true,
            },
            nomeResponsavel: {
                required: true,
            },
            cpfResponsavel: {
                required: true,
            },
            emailResponsavel: {
                required: true,
            },

            //Endereço
            cep: {
                required: true,
            },
            rua: {
                required: true,
            },
            numero: {
                required: true,
            },
            bairro: {
                required: true,
            },
            cidade: {
                required: true,
            },
            estado: {
                required: true,
            },
            complemento: {
                required: true,
            },
            pontoReferencia: {
                required: true,
            },

            //Contato
            nomeContato: {
                required: true,
            },
            emailContato: {
                required: true,
            },
            telefoneContato: {
                required: true,
            },
        }
    });

});



