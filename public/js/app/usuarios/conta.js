$(document).ready(function () {

    $("#formAlteracaoConta").validate({
        rules: {
            email: {
                required: true,
            },
            senha: {
                required: true,
            },
            confirmaSenha: {
                required: true,
                equalTo: '#senha',
            },
        }
    });

    $("#formAlteracaoContaDadosPessoais").validate({
        rules: {
            nome: {
                required: true,
            },
            sobrenome: {
                required: true,
            },
            cargo: {
                required: true,
            },
            area: {
                required: true,
            },
        }
    });


});



