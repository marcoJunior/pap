$(document).ready(function () {

    $("#editaTarefa").validate({
        rules: {
            tituloTarefa: {
                required: true,
            },
            descricaoTarefa: {
                required: true,
            },
            dataSolicita: {
                required: true,
            },
            dataUltimaAlteracao: {
                required: true,
            },
            status: {
                required: true,
            },
        }
    });

});



