$.fn.dataTable.ext.errMode = 'none';

var serviceGrid = function () {

    var self = this;
    var code;

    self.local;
    self.linhaSelecionada;
    self.linhaSelecionadaElement;
    self.linhaSelecionadaIndex = 0;
    self.resultado;
    self.dataTable;
    self.configuracaoGrid;

    this.criarBotaoEspecial = function (posicao, ordenavel, rendenizar) {
        this.colunasEspeciais.push({
            targets: posicao,
            orderable: ordenavel,
            render: function (data, type, row) {
                return rendenizar(data, type, row);
            }
        });
    };

    this.callBack = function (local) {
        $('input[type=checkbox]').uniform();
        load.esconde(local);
        if (typeof self.getDatarTable() !== "undefined" &&
                typeof self.getDatarTable().ajax.json().mensagem !== "undefined") {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: self.getDatarTable().ajax.json().mensagem,
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
        }


        setTimeout(function () {

            $(local).removeClass('table-bordered');
            $(local + ' tbody tr').eq(self.linhaSelecionadaIndex).click();
            $(local + '_filter > input').attr("tabindex", -1).focus();

            if ($('#grid').is(":visible") && !$('.dropdown-backdrop').is(":visible")) {
                $(local + ' tbody tr').eq(self.linhaSelecionadaIndex).click();
                $(local + ' tbody tr').eq(self.linhaSelecionadaIndex).attr("tabindex", -1).focus();
            }
        }, 200);
    }

    this.setConfiguracoes = function (local, url, colunas, colunasEspe, antesDoEnvio, enviarInformacoes, depoisDoEnvio) {
        self.configuracaoGrid = {
            'serverSide': true,
            'processing': false,
            'order': [[0, 'desc']],
            fixedHeader: {
                header: true,
                footer: true
            },
            'responsive': {
                'opcoes': {
                    'type': 'column',
                    'target': 'tr'
                },
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Detalhes (' + data[colunas[0]['data']] + " - " + data[colunas[1]['data']] + ')';
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table tabelaResponsiva'
                    })
                }
            },
            'oLanguage': self.getArrayLinguagemPortugues(),
            'ajax': {
                'url': url,
                'type': 'GET',
                'data': function (d) {
                    if (typeof antesDoEnvio === "function") {
                        antesDoEnvio();
                    }

                    load.mostra(local);
                    var paramData = {};
                    if (typeof enviarInformacoes === "function") {
                        paramData = enviarInformacoes();
                    }
                    return $.extend({}, d, {
                        paramData,
//                        'filtros': getFiltros(local.replace('#', '.') + '_Filtros')
                    });
                },
            },
            'drawCallback': function (settings) {
                self.callBack(local);
                if (typeof depoisDoEnvio === "function") {
                    depoisDoEnvio();
                }
            },
            'columns': colunas,
            'columnDefs': colunasEspe,
            'lengthMenu': [[10, 15, 20, 40, 100], [10, 15, 20, 40, 100]]
        };
    };

    this.carregarGrid = function (local, url, colunas, colunasEspe, postRecebido, antesDoEnvio, enviarInformacoes, depoisDoEnvio) {
        self.local = local;

        self.setConfiguracoes(local, url, colunas, colunasEspe, antesDoEnvio, enviarInformacoes, depoisDoEnvio);

        if (postRecebido !== null) {
            self.configuracaoGrid.ajax['dataSrc'] = postRecebido;
        }

        self.dataTable = $(local).DataTable(self.configuracaoGrid);
        self.dataTable.on('error.dt', function (e, settings, techNote, message) {
            console.log(e);
            console.log(message);
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: message,
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
        });

        this.setClassStyle(local, colunas);
        this.setEvents(local, colunas);
        this.setKeyDowns(local);

    };

    this.setClassStyle = function (local, colunas) {

        var filter = $(local + '_filter');
        var length = $(local + '_length');
        var FiltroGridBusca = $(local + '_FiltroGridBusca');
        var FiltroGridBuscaLeng = $(local + '_FiltroGridBuscaLeng');
        var FiltroGridBuscaColuna = $(local + '_FiltroGridBuscaColuna');

        $(local).css('width', '100%');
        $(local).parent().attr('style', 'min-height:200px;overflow-x:none;width: 100%;');

        if ((typeof $('#datatable').attr('class') !== "undefined" && $('#datatable').attr('class').indexOf('collapsed') <= -1) ||
                (typeof $(local).attr('class') !== "undefined" && $(local).attr('class').indexOf('collapsed') <= -1)) {
            $(local).parent().attr('class', 'table-responsive');
        }

        filter.appendTo(FiltroGridBusca);

        filter.find('input').attr('placeholder', 'Pesquisar...')
                .css('height', '33px').css('margin-left', '5px').css('margin-bottom', '5px').css('width', '225px');

        $('[name=colunaBusca]').css('margin-bottom', '5px');
        $('[name=formaBusca]').css('margin-bottom', '5px');

        filter.addClass('pull-left').removeClass('dataTables_filter');
        filter.find('input').appendTo(filter);

        filter.find('label').remove();
        FiltroGridBusca.appendTo($(local + '_Filtros'));

        length.appendTo(FiltroGridBuscaLeng);
        FiltroGridBuscaLeng.find('select').attr('class', 'form-control pull-left').css('margin-rigth', '5px');

        if (FiltroGridBuscaLeng.attr('mover') !== "false") {
            FiltroGridBuscaLeng.prependTo($(local + '_info').parent().parent());
        }

        length.css('margin-top', '2px').css('width', '70px');
        length.find('select').css('margin-left', '5px').css('margin-bottom', '5px');
        length.find('select').appendTo(length);
        length.find('label').remove();

        var optionsColumn = '';
        $.each(colunas, function (index, value) {
            if (typeof value['descricao'] !== "undefined") {
                optionsColumn += '<option tipo="' + value['tipo'] + '"' +
                        " value=" + value['data'] + ">" + value['descricao'] + " </option>";
            }
        });

        FiltroGridBuscaColuna.find('select').html(optionsColumn);

        filter.find('input').attr("type", FiltroGridBuscaColuna.find('select > option').eq(0).attr('tipo'));

        this.selecionarTipoPesquisa(local, colunas, FiltroGridBuscaColuna);
    };

    this.setEvents = function (local, colunas) {

        var trElement;
        var filter = $(local + '_filter');
        var tbody = $(local + ' tbody');
        var length = $(local + '_length');
        var FiltroGridBuscaColuna = $(local + '_FiltroGridBuscaColuna');


        length.on('change', function () {
            self.linhaSelecionadaIndex = 0;
            $(local).parent().scrollTop(-1);
        });

        tbody.on('click', 'tr', function () {
            $(local + ' tbody tr').removeClass('selected-row');
            $(this).addClass('selected-row');
            if (!$('.dtr-bs-modal .modal-content .modal-footer').length) {
                $('.dtr-bs-modal .modal-content').append(
                        '<div class="modal-footer">' +
                        '<center><button type="button" class="btn btn-large btn-success" data-dismiss="modal" aria-hidden="true"><i class="material-icons md-24 ">check</i> OK</button></center>' +
                        '</div>'
                        );
            }

            self.linhaSelecionada = self.getDatarTable().row(this).data();
            self.linhaSelecionadaIndex = self.getDatarTable().row(this).index();

            if ($(this).html().indexOf('input') < 0 && $(this).html().indexOf('select') < 0) {
                tbody.attr("tabindex", -1).focus();
            }

        });

        tbody.on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        tbody.on('dblclick', 'tr > td', function (e) {
            if ($(this).html().indexOf('input') < 0 && $(this).html().indexOf('select') < 0) {
                $(this).parent().find('.BtnDetalhes').click();
            }
        });

        FiltroGridBuscaColuna.on('change', 'select', function () {
            self.selecionarTipoPesquisa(local, colunas, FiltroGridBuscaColuna);
        });

        $(local + '_FiltroGridBuscaLimpar').on('click', function () {
            $(local + '_filter :input').val(null);
            self.dataTable.search('');
            self.dataTable.ajax.reload();
        });

        $(local + '_FiltroGridBuscaTipo').on('change', 'select', function () {
            self.dataTable.ajax.reload();
        });

        filter.find('input').attr("type", $(local + ' thead th').eq(0).attr("tipo"));

    };

    this.setKeyDowns = function (local) {

        var length = $(local + '_length');
        var tbody = $(local + ' tbody');

        $(local).keydown(function (e) {
            if ($('#grid').is(":visible") && code !== 32) {
                tbody.attr("tabindex", -1).focus();
            }

            if (!$('.blockUI.blockMsg.blockElement').is(":visible")) {

                code = (e.keyCode ? e.keyCode : e.which);

                console.log(code);

                switch (code) {

                    case 40:
                        if (self.linhaSelecionadaIndex < $(local + ' tbody tr').length - 1) {
                            self.linhaSelecionadaIndex = self.linhaSelecionadaIndex + 1;
                        } else {
                            self.linhaSelecionadaIndex = 0;
                            $(local + '_next').click();
                            $(local).parent().scrollTop(-1);
                        }
                        break;

                    case 38:

                        if (self.linhaSelecionadaIndex > 0) {
                            self.linhaSelecionadaIndex = self.linhaSelecionadaIndex - 1;
                        } else {
                            self.linhaSelecionadaIndex = length.find(":selected").text() - 1;
                            $(local + '_previous').click();
                            $(local).parent().scrollTop(tbody.height());
                        }

                        break;

                    case 37:

                        self.linhaSelecionadaIndex = length.find(":selected").text() - 1;
                        $(local + '_previous').click();
                        $(local).parent().scrollTop(tbody.height());

                        break;

                    case 39:

                        self.linhaSelecionadaIndex = 0;
                        $(local + '_next').click();
                        $(local).parent().scrollTop(-1);

                        break;

                    case 80:

                        $(local + '_BtnBusca').click();
                        $(local + '_filter :input').attr("tabindex", -1).focus();

                        break;

                    case 46:

                        //backspace keycode
                        if (typeof self.linhaSelecionadaElement !== "undefined") {
                            $(self.linhaSelecionadaElement).find('.BtnExcluir').click();
                        }

                        break;
                }


                if ((code === 37 || code === 39) && self.linhaSelecionadaIndex >= 0 && self.linhaSelecionadaIndex <= $(local + ' tbody tr').length - 1) {
                    self.linhaSelecionada = self.getDatarTable().row(self.linhaSelecionadaIndex).data();
                    self.linhaSelecionadaElement = $(local + ' tbody tr').eq(self.linhaSelecionadaIndex);
                } else if ((code === 38 || code === 40) && self.linhaSelecionadaIndex >= 0 && self.linhaSelecionadaIndex <= $(local + ' tbody tr').length - 1) {

                    self.linhaSelecionada = self.getDatarTable().row(self.linhaSelecionadaIndex).data();
                    self.linhaSelecionadaElement = $(local + ' tbody tr').eq(self.linhaSelecionadaIndex);
                    if (typeof self.linhaSelecionada !== "undefined") {
                        $(self.linhaSelecionadaElement).click();
                    }
                }

                if (code === 32) {
                    //backspace keycode
                    if (typeof self.linhaSelecionadaElement !== "undefined") {
                        $(self.linhaSelecionadaElement).find('.BtnDetalhes').click();
                    }
                }

            } else {
                if (code === 37) {
                    $(local).parent().scrollTop(tbody.height());
                } else if (code === 39) {
                    $(local).parent().scrollTop(-1);
                }
            }
        });
    };

    this.selecionarTipoPesquisa = function (local, colunas, type) {

        var optionsText = '<option value="0">Contendo</option>' +
                '<option value="1">Iniciado</option>';

        var optionsNumber = '<option value="equalTo">= Igual</option>' +
                '<option value="lessThan">&lt; Menor</option>' +
                '<option value="lessThanOrEqualTo">≦ Menor ou igual</option>' +
                '<option value="greaterThan">&gt; Maior</option>' +
                '<option value="greaterThanOrEqualTo">≧ Maior ou igual</option>';

        $(local + '_filter').find('input').show();
        var tipo = type.find(":selected").attr("tipo");
        var opcoes = '';

        switch (tipo) {
            case "text":
                opcoes = optionsText;
                break;

            case "number" || "date":
                $(local + '_filter').find('input').show();
                opcoes = optionsNumber;
                break;

            case "select" || "sim/nao":
                opcoes = '';
                var coluna = $(this).find(":selected").attr('value');

                $.each(colunas, function (index, value) {
                    if (value.data === coluna) {
                        $.each(value.options, function (i, val) {
                            opcoes += '<option value="equalTo" valor="' + i + '">' + val + '</option>';
                        });
                    }
                });
                $(local + '_filter').find('input').hide();

                $(local + '_FiltroGridBuscaTipo > select').on('change', function () {
                    var valor = $(this).find(":selected").attr("valor");
                    $(local + '_filter :input').val(valor);
                    self.dataTable.search(valor);
                    self.dataTable.ajax.reload();
                });

                break;
        }

        $(local + '_FiltroGridBuscaTipo > select').html(opcoes);

        if (tipo !== "select") {
            $(local + '_filter').find('input').attr("type", tipo);
            $(local + '_filter :input').val(null);
            self.dataTable.search('');
            self.dataTable.ajax.reload();
        }


    };

    this.getLinhaSelectElement = function () {
        return self.linhaSelecionadaElement;
    };
    this.getLinhaSelectDb = function () {
        return self.linhaSelecionada;
    };
    this.getLinhaSelectIndex = function () {
        return self.linhaSelecionadaIndex;
    };
    this.setLinhaSelectIndex = function (valor) {
        self.linhaSelecionadaIndex = valor;
    };
    this.getDatarTable = function () {
        return self.dataTable;
    };
    this.getDatarGrid = function () {
        return self.resultado;
    };
    this.getArrayLinguagemPortugues = function () {
        return {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Aguarde carregando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        };
    };
};

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);
    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

$(document).ready(function () {

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 27) {
            $('.BtnVoltar').click();
        }
        if (code === 107) { // && e.altKey) {
            $('.btnAdicionar').click();
        }
    });
    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });
});
