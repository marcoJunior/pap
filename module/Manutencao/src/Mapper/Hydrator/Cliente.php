<?php

namespace Manutencao\Mapper\Hydrator;

use Manutencao\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Cliente extends Hydrator
{

    protected function getEntity()
    {
        return 'Manutencao\Entity\Cliente';
    }

    public function getMap()
    {
        $arrayMap = [
            // 'razaoSocial' => 'razao_social'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Cliente();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
