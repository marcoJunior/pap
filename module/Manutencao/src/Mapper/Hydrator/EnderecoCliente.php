<?php

namespace Manutencao\Mapper\Hydrator;

use Manutencao\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class EnderecoCliente extends Hydrator
{

    protected function getEntity()
    {
        return 'Manutencao\Entity\EnderecoCliente';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new EnderecoCliente();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
