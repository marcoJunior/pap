<?php

namespace Manutencao\Mapper;

use Zend\Db\Sql\Where;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use Manutencao\Entity\ContatoCliente as ContatoClienteEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ContatoCliente extends APIGrid
{

    public $tableName = 'contato';
    public $mapperName = 'Manutencao\Mapper\Hydrator\ContatoCliente';

    public function getColunas()
    {
        return [
            'nome',
            'telefone',
            'email',
            'id_cliente'
        ];
    }

    public function validaExistencia(ContatoClienteEntity $entity)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $entity->getidCliente());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(ContatoClienteEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(ContatoClienteEntity $entity)
    {
        if ($this->validaExistencia($entity)) {

            $where = new Where();
            $where->equalTo('id_cliente',
                    $entity->getidCliente());

            return $this->update($entity, $where)->getAffectedRows();
        }

        return $this->adicao($entity);
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id_cliente', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
