<?php

namespace Cliente\Mapper;

use Zend\Db\Sql\Where;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use Cliente\Entity\EnderecoCliente as EnderecoClienteEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class EnderecoCliente extends APIGrid
{

    public $tableName = 'endereco';
    public $mapperName = 'Cliente\Mapper\Hydrator\EnderecoCliente';

    public function getColunas()
    {
        return [
            'cep',
            'rua',
            'numero',
            'complemento',
            'bairro',
            'cidade',
            'estado'
        ];
    }

    public function validaExistencia(EnderecoClienteEntity $entity)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $entity->getidCliente());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(EnderecoClienteEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(EnderecoClienteEntity $entity)
    {
        if ($this->validaExistencia($entity)) {

            $where = new Where();
            $where->equalTo('id_cliente',
                    $entity->getidCliente());

            return $this->update($entity, $where)->getAffectedRows();
        }

        return $this->adicao($entity);
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id_cliente', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
