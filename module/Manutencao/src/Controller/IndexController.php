<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manutencao\Controller;

use APIGridGitLab\Controller\APIGridController;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;

class IndexController extends APIGridController
{

    public function getList()
    {
        die('aqui');
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewCliente = new Cliente($phpRenderer);
            $viewCliente->setVariable('titulo', 'Clientes');
            $viewCliente->grid();
//            $viewCliente->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewCliente);

            return $app;
        }

        $service = $this->serviceEmpresasMax();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceEmpresasMax();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceEmpresasMax();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceEmpresasMax();
        $get = new Parameters($data);

//        var_dump($get);die();

        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceEmpresasMax();
        return new JsonModel((array) $service->excluir($id));
    }

//    public function clientesAction()
//    {
//        die('aqui');
//        $login = new ViewModel();
//        $login->setTemplate('clientes/index');
//        return $login;
//    }
//
    public function produtosAction()
    {
        $login = new ViewModel();
        $login->setTemplate('produtos/portifolio');
        return $login;
    }

     /**
     * @return \Manutencao\Service\Clientes
     */
    public function serviceClientes()
    {
        return $this->getService()->get(\Manutencao\Service\Clientes::class);
    }

     /**
     * @return \Manutencao\Service\Produtos
     */
    public function serviceProdutos()
    {
        return $this->getService()->get(\Manutencao\Service\Produtos::class);
    }

}
