<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manutencao;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Cliente::class => function ($sm) {
                    $service = new Service\Cliente();
                    return $service->setServiceManager($sm);
                },
                'Manutencao\Mapper\Cliente' =>
                function ($sm) {
                    $mapper = new Mapper\Cliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ClienteCompleto())
                        ->setHydrator(new Mapper\Hydrator\ClienteCompleto());
                    return $mapper;
                },
                'Manutencao\Mapper\ContatoCliente' =>
                function ($sm) {
                    $mapper = new Mapper\ContatoCliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ContatoCliente())
                        ->setHydrator(new Mapper\Hydrator\ContatoCliente());
                    return $mapper;
                },
                'Manutencao\Mapper\EnderecoCliente' =>
                function ($sm) {
                    $mapper = new Mapper\EnderecoCliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\EnderecoCliente())
                        ->setHydrator(new Mapper\Hydrator\EnderecoCliente());
                    return $mapper;
                },
            ),
        );
    }

}
