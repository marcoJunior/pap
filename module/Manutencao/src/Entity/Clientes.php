<?php

class Clientes {

    protected $id;
    protected $nome;
    protected $sobre_nome;
    protected $idade;
    protected $sexo;
    protected $enderecos_id;
    protected $contatos_id;

    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getSobre_nome() {
        return $this->sobre_nome;
    }

    function getIdade() {
        return $this->idade;
    }

    function getSexo() {
        return $this->sexo;
    }

    function getEnderecos_id() {
        return $this->enderecos_id;
    }

    function getContatos_id() {
        return $this->contatos_id;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSobre_nome($sobre_nome) {
        $this->sobre_nome = $sobre_nome;
    }

    function setIdade($idade) {
        $this->idade = $idade;
    }

    function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    function setEnderecos_id($enderecos_id) {
        $this->enderecos_id = $enderecos_id;
    }

    function setContatos_id($contatos_id) {
        $this->contatos_id = $contatos_id;
    }
    
    function __construct($id, $nome, $sobre_nome) {
        $this->id = $id;
        $this->nome = $nome;
        $this->sobre_nome = $sobre_nome;
    }


}
