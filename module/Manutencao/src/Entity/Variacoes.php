<?php

class Variacoes {

    protected $id;
    protected $preco;
    protected $descricao;
    protected $id_produtos;
    protected $id_grupos;

    function getId() {
        return $this->id;
    }

    function getPreco() {
        return $this->preco;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getId_produtos() {
        return $this->id_produtos;
    }

    function getId_grupos() {
        return $this->id_grupos;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPreco($preco) {
        $this->preco = $preco;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setId_produtos($id_produtos) {
        $this->id_produtos = $id_produtos;
    }

    function setId_grupos($id_grupos) {
        $this->id_grupos = $id_grupos;
    }

    function __construct($id, $preco, $id_produtos, $id_grupos) {
        $this->id = $id;
        $this->preco = $preco;
        $this->id_produtos = $id_produtos;
        $this->id_grupos = $id_grupos;
    }

}
