<?php

namespace Manutencao\Entity;

use Traversable;

abstract class AbstractEntity
{

    function __construct($data = [])
    {
        if (is_array($data) || $data instanceof Traversable) {
            $this->exchangeArray($data);
        }
    }

    public function exchangeArray($data)
    {
        if (!(is_array($data) || $data instanceof Traversable)) {
            throw new \InvalidArgumentException('O parametro deve ser array ou Traversable');
        }

        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Garante para a propriedade que irá retornar a classe desejada
     * @param midex $propriedade
     * @param object $entity
     * @return $objectName
     */
    public function returnEntity($propriedade, $entity)
    {
        return ($propriedade instanceof $entity ? $propriedade : new $entity([]));
    }

    public function __isset($name)
    {
        return property_exists($this, $name);
    }

}
