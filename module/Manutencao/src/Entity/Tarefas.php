<?php

class Tarefas {

    protected $id;
    protected $descricao;
    protected $nivel;
    protected $id_usuarios;
    protected $id_cargos_usuarios;

    function getId() {
        return $this->id;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getNivel() {
        return $this->nivel;
    }

    function getId_usuarios() {
        return $this->id_usuarios;
    }

    function getId_cargos_usuarios() {
        return $this->id_cargos_usuarios;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }

    function setId_usuarios($id_usuarios) {
        $this->id_usuarios = $id_usuarios;
    }

    function setId_cargos_usuarios($id_cargos_usuarios) {
        $this->id_cargos_usuarios = $id_cargos_usuarios;
    }

    function __construct($id, $descricao, $nivel, $id_usuarios, $id_cargos_usuarios) {
        $this->id = $id;
        $this->descricao = $descricao;
        $this->nivel = $nivel;
        $this->id_usuarios = $id_usuarios;
        $this->id_cargos_usuarios = $id_cargos_usuarios;
    }

}
