<?php

namespace Manutencao\Entity;

use Manutencao\Entity\AbstractEntity;

class Cliente extends AbstractEntity
{

    protected $id;
    protected $serie;
    protected $razaoSocial = '';
    protected $fantasia;
    protected $documento = '';
    protected $logo = '';
    protected $ativo = 0;

    function getSerie()
    {
        return $this->serie;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function getId()
    {
        return $this->id;
    }

    function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    function getFantasia()
    {
        return $this->fantasia;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getLogo()
    {
        return $this->logo;
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
    }

    function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setLogo($logo)
    {
        $this->logo = $logo;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        if (gettype($ativo) == 'string') {
            $this->ativo = $ativo == 'true' ? 1 : 0;
        }
    }

}
