<?php

namespace APIGridGitLab\Controller;

use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractRestfulController;

/**
 * Controller para auxilio há criação de grids
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGridController extends AbstractRestfulController
{

    protected $service;

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * Metodo criado para capturar $_GET em uma segunda requisição por outra url.
     * @return array
     */
    function getRequestQuery()
    {
        $get = [];
        if (isset(explode('?', $GLOBALS['_SERVER']['HTTP_REFERER'])[1])) {
            parse_str(explode('?', $GLOBALS['_SERVER']['HTTP_REFERER'])[1], $get);
        }

        return $get;
    }

}
