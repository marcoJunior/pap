var serviceGridEmpresas = new serviceGrid();
var initEmpresas = new function () {

    this.colunasEmpresas = [
        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Razão social', 'tipo': 'text', 'data': 'razaoSocial'},
        {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'fantasia'},
        {'descricao': 'Documento', 'tipo': 'text', 'data': 'documento'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ];

    this.colunasEspeEmpresas = [
        {
            'targets': [5],
            'orderable': false,
            "render": function (data, type, row) {
                var fantasia = "'" + row.fantasia + "'";
                var btnConsultar = '<button type="button" ' +
                        ' onclick="initEmpresas.clickBotaoConsultar('
                        + row.id + ',' + fantasia +
                        ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                        '<i class="font-white fa fa-info"></i></button>';

                return '<center>' + btnConsultar + '</center>';

            }
        },
        {
            'targets': [6],
            'orderable': false,
            "render": function (data, type, row) {
                var fantasia = "'" + row.fantasia + "'";
                var btnExcluir = '<button type="button" ' +
                        ' onclick="initEmpresas.confirmarExclusaoRegistro('
                        + row.id + ',' + fantasia +
                        ');" class="btn btn-danger btn-xs BtnExcluir">' +
                        '<i class="fa fa-close"></i></button>';

                return '<center>' + btnExcluir + '</center>';

            }
        }
    ];

    this.iniciarGridEmpresas = function () {

        serviceGridEmpresas.carregarGrid(
                '#datatable',
                location.pathname + '/selecionar', //URL DO SELECT
                this.colunasEmpresas, //COLUNAS DO GRID E PARA PESQUISA
                this.colunasEspeEmpresas, //COLUNAS ESPECIAS COM ACOES OU FORMATAÇÕES ESPECIAS PARA O GRID
                null, //eNVIAR INFORMAÇÃO PATA HÁ TABELA 
                function () {
                    //FUÇÃO EXECUTADA ANTES DA REQUISIÇÃO
                },
                {
                    //ENVIAR OBJETO DE INFORMAÇÕES PARA A REQUISIÇÃO AJAX
                }
        );
    };

    this.iniciarGridEmpresas();

};

