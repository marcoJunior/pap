var serviceGrid = function () {
    var self = this;
    var code;

    self.linhaSelecionada;
    self.linhaSelecionadaElement;
    self.linhaSelecionadaIndex = 0;
    self.resultado;
    self.dataTable;
    self.configuracaoGrid;

    this.criarBotaoEspecial = function (posicao, ordenavel, rendenizar) {
        this.colunasEspeciais.push({
            targets: posicao,
            orderable: ordenavel,
            render: function (data, type, row) {
                return rendenizar(data, type, row);
            }
        });
    };

    this.carregarGrid = function (local, url, colunas, colunasEspe, postRecebido, antesDoEnvio, enviarInformacoes) {

        self.configuracaoGrid = {
            'serverSide': true,
            'processing': false,
            'order': [[0, 'desc']],
            'responsive': {
                'opcoes': {
                    'type': 'column',
                    'target': 'tr'
                },
                details: {
                    display: $.fn.dataTable.Responsive.display.modal({
                        header: function (row) {
                            var data = row.data();
                            return 'Detalhes (' +
                                    data[colunas[0]['data']] +
                                    " - " +
                                    data[colunas[1]['data']]
                                    + ')';
                        }
                    }),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                        tableClass: 'table'
                    })
                }
            },
            'oLanguage': self.getArrayLinguagemPortugues(),
            'ajax': {
//                'timeout': 4500,
                'url': url,
                'type': 'POST',
                'data': function (d) {
                    if (typeof antesDoEnvio === "function") {
                        antesDoEnvio();
                    }

                    load.mostra(local);

                    var paramData = {};
                    if (typeof enviarInformacoes === "function") {
                        paramData = enviarInformacoes();
                    }
                    return $.extend({}, d, {
                        paramData,
                        'filtros': getFiltros(local.replace('#', '.') + '_Filtros')
                    });
                },
            },
            'drawCallback': function (settings) {
                load.esconde(local);
                if (typeof self.getDatarTable() !== "undefined" &&
                        typeof self.getDatarTable().ajax.json().mensagem !== "undefined") {
                    VanillaToasts.create({
                        title: 'Alerta de erro !',
                        text: self.getDatarTable().ajax.json().mensagem,
                        type: 'error', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }


                setTimeout(function () {
                    $(local).removeClass('table-bordered');
                    if ($('#grid').is(":visible") && !$('.dropdown-backdrop').is(":visible")) {
                        $(local + ' tbody tr').eq(self.linhaSelecionadaIndex).click();
                        $(local + ' tbody tr').eq(self.linhaSelecionadaIndex).attr("tabindex", -1).focus();
                    }
                }, 200);
            },
            'columns': colunas,
            'columnDefs': colunasEspe,
            'lengthMenu': [[10, 15, 20, 40, 100], [10, 15, 20, 40, 100]]
        };

        if (postRecebido !== null) {
            self.configuracaoGrid.ajax['dataSrc'] = postRecebido;
        }

        self.dataTable = $(local).DataTable(self.configuracaoGrid);

        $.fn.dataTable.ext.errMode = 'none';
        setTimeout(function () {
            self.dataTable.on('error.dt', function (e, settings, techNote, message) {

                console.log(message);
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: message,
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
            });
        }, 100);

        $(local + '_length').on('change', function () {
            self.linhaSelecionadaIndex = 0;
            $(local).parent().scrollTop(-1);
        });

        $(local + ' tbody').on('click', 'tr', function () {
            $(local + ' tbody tr').removeClass('selected-row');
            $(this).addClass('selected-row');

            self.linhaSelecionada = self.getDatarTable().row(this).data();
            self.linhaSelecionadaIndex = self.getDatarTable().row(this).index();

            $(local + ' tbody').attr("tabindex", -1).focus();
        });

        var trElement;
        $(local + ' tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $(local + ' tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

        $(local).css('width', '100%');

        if (typeof $('#datatable').attr('class') !== "undefined" && $('#datatable').attr('class').indexOf('collapsed') <= -1) {
            $(local).parent().attr('class', 'table-responsive');
            $(local).parent().attr('style', 'min-height:200px;max-height:600px;width: 100%;overflow-x:none;');
        } else {
            $(local).parent().attr('style', 'min-height:200px;overflow-x:none;width: 100%;');
        }

        $(local + '_processing').attr('style', 'padding-bottom: 15px;');
        $(local).parent().append($('</div id="' + local.replace('#', '') + '_loadCenter">'));

        $(local + '_filter').appendTo($(local + '_FiltroGridBusca'));
        $(local + '_filter').find('input').attr('placeholder', 'Pesquisar...')
                .css('height', '33px')
                .css('margin-left', '5px')
                .css('margin-bottom', '5px')
                .css('width', '225px');

        $('[name=colunaBusca]').css('margin-bottom', '5px');
        $('[name=formaBusca]').css('margin-bottom', '5px');

        $(local + '_filter')
                .addClass('pull-left')
                .removeClass('dataTables_filter');

        $(local + '_filter').find('input')
                .appendTo($(local + '_filter'));
        $(local + '_filter').find('label').remove();
        $(local + '_FiltroGridBusca').appendTo($(local + '_Filtros'));

        if (typeof $(local + '_FiltroGridBuscaLeng') != "undefined") {
            // $(local + '_info').parent().removeClass('col-sm-5').addClass('col-sm-4');
        }
        $(local + '_length').appendTo($(local + '_FiltroGridBuscaLeng'));
        $(local + '_FiltroGridBuscaLeng').find('select').attr('class', 'form-control pull-left')
                .css('margin-rigth', '5px');
        if ($(local + '_FiltroGridBuscaLeng').attr('mover') !== "false") {
            $(local + '_FiltroGridBuscaLeng').prependTo($(local + '_info').parent().parent());
        }
        $(local + '_length').css('margin-top', '2px');
        $(local + '_length').css('width', '70px');
        $(local + '_length').find('select').css('margin-left', '5px')
                .css('margin-bottom', '5px');

        $(local + '_length').find('select').appendTo($(local + '_length'));
        $(local + '_length').find('label').remove();

        $.each(colunas, function (index, value) {
            if (typeof value['descricao'] !== "undefined") {
                $(local + '_FiltroGridBuscaColuna > select').append("<option" +
                        ' tipo="' + value['tipo'] + '"' +
                        " value=" + value['data'] + ">" + value['descricao'] +
                        " </option>");
            }
        });

        $(local + '_filter').find('input').attr("type",
                $(local + '_FiltroGridBuscaColuna > select > option').eq(0).attr('tipo')
                );
        $(local + '_FiltroGridBuscaTipo > select').html('' +
                '<option value="equalTo">= Igual</option>' +
                '<option value="lessThan">&lt; Menor</option>' +
                '<option value="lessThanOrEqualTo">≦ Menor ou igual</option>' +
                '<option value="greaterThan">&gt; Maior</option>' +
                '<option value="greaterThanOrEqualTo">≧ Maior ou igual</option>'
                );

        $(local + '_FiltroGridBuscaColuna').on('change', 'select', function () {
            $(local + '_filter').find('input').show();
            var tipo = $(this).find(":selected").attr("tipo");
            if (tipo === "text") {
                $(local + '_FiltroGridBuscaTipo > select').html('' +
                        '<option value="0">Contendo</option>' +
                        '<option value="1">Iniciado</option>'
                        );
            } else if (tipo === "number" || tipo === "date") {
                $(local + '_filter').find('input').show();
                $(local + '_FiltroGridBuscaTipo > select').html('' +
                        '<option value="equalTo">= Igual</option>' +
                        '<option value="lessThan">&lt; Menor</option>' +
                        '<option value="lessThanOrEqualTo">≦ Menor ou igual</option>' +
                        '<option value="greaterThan">&gt; Maior</option>' +
                        '<option value="greaterThanOrEqualTo">≧ Maior ou igual</option>'
                        );
            } else if (tipo === "select" || tipo === "sim/nao") {
                var opcoes = '';
                var coluna = $(this).find(":selected").attr('value');
                $.each(colunas, function (index, value) {
                    if (value.data == coluna) {
                        $.each(value.options, function (i, val) {
                            opcoes += '<option value="equalTo" valor="' + i + '">' + val + '</option>';
                        });
                    }
                });

                $(local + '_filter').find('input').hide();
                $(local + '_FiltroGridBuscaTipo > select').html(opcoes);
                $(local + '_FiltroGridBuscaTipo > select').on('change', function () {
                    var valor = $(this).find(":selected").attr("valor");
                    $(local + '_filter :input').val(valor);
                    self.dataTable.search(valor);
                    self.dataTable.ajax.reload();
                });
            }

            if (tipo !== "select") {
                $(local + '_filter').find('input').attr("type", tipo);
                $(local + '_filter :input').val(null);

                self.dataTable.search('');
                self.dataTable.ajax.reload();
            }

        });

        $(local + '_FiltroGridBuscaLimpar').on('click', function () {
            $(local + '_filter :input').val(null);
            self.dataTable.search('');

            self.dataTable.ajax.reload();
        });

        $(local + '_FiltroGridBuscaTipo').on('change', 'select', function () {
            self.dataTable.ajax.reload();
        });

        $(local + '_filter').find('input').attr("type", $(local + ' thead th').eq(0).attr("tipo"));


        $(local).keydown(function (e) {
            if ($('#grid').is(":visible") && code !== 32) {
                $(local + ' tbody').attr("tabindex", -1).focus();
            }

            if (!$(local + '_processing').is(":visible")) {

                code = (e.keyCode ? e.keyCode : e.which);

                if (code == 40) {
                    if (self.linhaSelecionadaIndex < $(local + ' tbody tr').length - 1) {
                        self.linhaSelecionadaIndex = self.linhaSelecionadaIndex + 1;
                    } else {
                        self.linhaSelecionadaIndex = 0;
                        $(local + '_next').click();
                        $(local).parent().scrollTop(-1);
                    }

                } else if (code == 38) {
                    if (self.linhaSelecionadaIndex > 0) {
                        self.linhaSelecionadaIndex = self.linhaSelecionadaIndex - 1;
                    } else {
                        self.linhaSelecionadaIndex = $(local + '_length').find(":selected").text() - 1;
                        $(local + '_previous').click();
                        $(local).parent().scrollTop($(local + ' tbody').height());
                    }
                } else if (code == 37) {
                    self.linhaSelecionadaIndex = $(local + '_length').find(":selected").text() - 1;
                    $(local + '_previous').click();
                    $(local).parent().scrollTop($(local + ' tbody').height());
                } else if (code == 39) {
                    self.linhaSelecionadaIndex = 0;
                    $(local + '_next').click();
                    $(local).parent().scrollTop(-1);
                } else if (code == 80) {
                    $(local + '_BtnBusca').click();
                    $(local + '_filter :input').attr("tabindex", -1).focus();
                } else if (code == 46) {
                    //backspace keycode
                    if (typeof self.linhaSelecionadaElement !== "undefined") {
                        $(self.linhaSelecionadaElement).find('.BtnExcluir').click();
                    }
                }
                //console.log(code);

                if ((code == 37 || code == 39) && self.linhaSelecionadaIndex >= 0 && self.linhaSelecionadaIndex <= $(local + ' tbody tr').length - 1) {
                    self.linhaSelecionada = self.getDatarTable().row(self.linhaSelecionadaIndex).data();
                    self.linhaSelecionadaElement = $(local + ' tbody tr').eq(self.linhaSelecionadaIndex);
                } else if ((code == 38 || code == 40) && self.linhaSelecionadaIndex >= 0 && self.linhaSelecionadaIndex <= $(local + ' tbody tr').length - 1) {

                    self.linhaSelecionada = self.getDatarTable().row(self.linhaSelecionadaIndex).data();
                    self.linhaSelecionadaElement = $(local + ' tbody tr').eq(self.linhaSelecionadaIndex);

                    if (typeof self.linhaSelecionada !== "undefined") {
                        $(self.linhaSelecionadaElement).click();
                    }
                }

                if (code == 32) {
                    //backspace keycode
                    if (typeof self.linhaSelecionadaElement !== "undefined") {
                        $(self.linhaSelecionadaElement).find('.BtnDetalhes').click();
                    }
                }

            } else {
                if (code == 37) {
                    $(local).parent().scrollTop($(local + ' tbody').height());
                } else if (code == 39) {
                    $(local).parent().scrollTop(-1);
                }
            }
        });

    };

    this.getLinhaSelectElement = function () {
        return self.linhaSelecionadaElement;
    };

    this.getLinhaSelectDb = function () {
        return self.linhaSelecionada;
    };

    this.getLinhaSelectIndex = function () {
        return self.linhaSelecionadaIndex;
    };
    this.setLinhaSelectIndex = function (valor) {
        self.linhaSelecionadaIndex = valor;
    };
    this.getDatarTable = function () {
        return self.dataTable;
    };
    this.getDatarGrid = function () {
        return self.resultado;
    };

    this.getArrayLinguagemPortugues = function () {
        return {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Aguarde carregando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        };
    };

};

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

$(document).ready(function () {

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('.btnAdicionar').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

});
