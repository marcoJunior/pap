<?php

namespace APIGrid\Entity\Action;

use APIHelper\Entity\AbstractEntity;

/**
 * Entidade para padronização de join para os grid's
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGridJoin extends AbstractEntity
{

    protected $tabela;
    protected $condicao;
    protected $colunas;
    protected $forma;

    function __construct($tabela, $condicao, $colunas = [], $forma = '')
    {
        $this->tabela = $tabela;
        $this->condicao = $condicao;
        $this->colunas = $colunas;
        $this->forma = $forma;
    }

    function getTabela()
    {
        return $this->tabela;
    }

    function getCondicao()
    {
        return $this->condicao;
    }

    function getColunas()
    {
        return $this->colunas;
    }

    function getForma()
    {
        return $this->forma;
    }

    function setTabela($tabela)
    {
        $this->tabela = $tabela;
    }

    function setCondicao($condicao)
    {
        $this->condicao = $condicao;
    }

    function setColunas($colunas)
    {
        $this->colunas = $colunas;
    }

    function setForma($forma)
    {
        $this->forma = $forma;
    }

}
