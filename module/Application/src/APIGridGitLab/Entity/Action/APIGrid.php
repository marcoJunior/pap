<?php

namespace APIGrid\Entity\Action;

use APIHelper\Entity\AbstractEntity;

/**
 * Entidade para padronização de filtros para os grid's
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGrid extends AbstractEntity
{

    public $busca;
    public $buscaTipo;
    public $ordenacao;
    public $direcaoOrdenacao;
    public $colunaBuscaTipo;
    public $limit;
    public $offset;
    public $nextOrdenacao = '';
    protected $formaBusca;
    protected $colunaBusca;
    protected $draw = 1;

    public static function get()
    {
        return new APIGrid();
    }

    function getBusca()
    {
        return $this->busca;
    }

    function getOrdenacao()
    {
        return $this->ordenacao;
    }

    function getDirecaoOrdenacao()
    {
        return $this->direcaoOrdenacao;
    }

    function getLimit()
    {
        return $this->limit;
    }

    function getOffset()
    {
        return $this->offset;
    }

    function setBusca($busca)
    {
        $this->busca = $busca;
    }

    function setOrdenacao($ordenacao)
    {
        $this->ordenacao = $ordenacao;
    }

    function setDirecaoOrdenacao($direcaoOrdenacao)
    {
        $this->direcaoOrdenacao = $direcaoOrdenacao;
    }

    function setLimit($limit)
    {
        $this->limit = $limit;
    }

    function setOffset($offset)
    {
        $this->offset = $offset;
    }

    function getNextOrdenacao()
    {
        return $this->nextOrdenacao;
    }

    function setNextOrdenacao($nextOrdenacao)
    {
        $this->nextOrdenacao = $nextOrdenacao;
    }

    function getBuscaTipo()
    {
        return $this->buscaTipo;
    }

    function setBuscaTipo($buscaTipo)
    {
        $this->buscaTipo = $buscaTipo;
    }

    function getFormaBusca()
    {
        return $this->formaBusca;
    }

    function getColunaBusca()
    {
        return $this->colunaBusca;
    }

    function setFormaBusca($formaBusca)
    {
        $this->formaBusca = $formaBusca;
    }

    function setColunaBusca($colunaBusca)
    {
        $this->colunaBusca = $colunaBusca;
    }

    function getDraw()
    {
        return $this->draw;
    }

    function setDraw($draw)
    {
        $this->draw = $draw;
    }

    function getColunaBuscaTipo()
    {
        return $this->colunaBuscaTipo;
    }

    function setColunaBuscaTipo($colunaBuscaTipo)
    {
        $this->colunaBuscaTipo = $colunaBuscaTipo;
    }

}
