<?php

namespace APIGrid\Entity;

use APIHelper\Entity\AbstractEntity;

/**
 * Entidade para padronização de retorno dos grid's
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGridReturn extends AbstractEntity
{

    protected $draw;
    protected $recordsTotal;
    protected $recordsFiltered;
    protected $data;

    function __construct($draw, $recordsTotal, $recordsFiltered, $data)
    {
        $this->draw = $draw;
        $this->recordsTotal = $recordsTotal;
        $this->recordsFiltered = $recordsFiltered;
        $this->data = $data;
    }

    function getDraw()
    {
        return $this->draw;
    }

    function getRecordsTotal()
    {
        return $this->recordsTotal;
    }

    function getRecordsFiltered()
    {
        return $this->recordsFiltered;
    }

    function getData()
    {
        return $this->data;
    }

    function setDraw($draw)
    {
        $this->draw = $draw;
    }

    function setRecordsTotal($recordsTotal)
    {
        $this->recordsTotal = $recordsTotal;
    }

    function setRecordsFiltered($recordsFiltered)
    {
        $this->recordsFiltered = $recordsFiltered;
    }

    function setData($data)
    {
        $this->data = $data;
    }

}
