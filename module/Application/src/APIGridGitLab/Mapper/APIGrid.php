<?php

namespace APIGrid\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use ZfcBase\Mapper\AbstractDbMapper;
//APIGrid
use APIGrid\Entity\APIGridReturn as APIGridReturnEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntityAction;

/**
 * Mapper para auxilio há criação de grids
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGrid extends AbstractDbMapper
{

    public $linhaTotalizadora = [];
    public $colunasTotalizador = ['*'];
    public $tableName = '';
    public $hydartor = '';
    public $where = Null;
    public $from = Null;
    public $join = Null;
    public $contadorJoin = [];
    public $colunas = ['*'];
    public $sqlString = '';
    public $limitOffset = Null;
    public $groupBy = Null;

    /**
     * Metodo costrutor para utilização do getResultadoDb, nessecario, para identificar
     * tabela para o select,hydrator para a mesma e colunas para retorno
     * @param String $tableName
     * @param String $hydartor
     * @param Array $colunas
     */
    function inicializar($tableName, $hydartor, $colunas = ['*'])
    {
        $this->tableName = $tableName;
        $this->colunas = $colunas;
        $this->hydartor = $hydartor;
    }

    function getSqlString()
    {
        return $this->sqlString;
    }

    function setSqlString($sqlString)
    {
        $this->sqlString = $sqlString;
    }

    function getJoin()
    {
        return $this->join;
    }

    function getTableName()
    {
        return $this->tableName;
    }

    function getColunas()
    {
        return $this->colunas;
    }

    function getHydartor()
    {
        return $this->hydartor;
    }

    function getWhere()
    {
        return $this->where;
    }

    function getFromPersonalisado()
    {
        return $this->from;
    }

    function getLimitOffset()
    {
        return $this->limitOffset;
    }

    function getGroupBy()
    {
        return $this->groupBy;
    }

    function getContadorJoin()
    {
        return $this->contadorJoin;
    }

    function setContadorJoin($contadorJoin)
    {
        $this->contadorJoin = $contadorJoin;
    }

    function getLinhaTotalizadora()
    {
        return $this->linhaTotalizadora;
    }

    function setLinhaTotalizadora($linhaTotalizadora)
    {
        $this->linhaTotalizadora = $linhaTotalizadora;
    }

    function getColunasTotalizador()
    {
        return $this->colunasTotalizador;
    }

    function setColunasTotalizador($colunasTotalizador)
    {
        $this->colunasTotalizador = $colunasTotalizador;
    }

    /**
     * Condições enclausuradas, por setJoin
     * OBS:(Cada objeto do tipo Array sera colocado para ser executado
     * juntamente de proximas condições adicionas por setJoin's sub-sequentes)
     *
     * @param Array $join OBS: O array esperado deve conter 3 valores nos quais são :
     * ['TABELA', 'CONDIÇÃO DO JOIN', ['COLUNA1','COLUNA2','COLUNAS HA SEREM ADICIONADAS AO SELECT GERAL']]
     * EXEMPLO:['mxclicfi', 'mxpedcfi.cficliped = mxclicfi.cficodcli', ['cficodcli','cfinomcli']]
     */
    function setJoin(APIGridJoinEntityAction $join)
    {
        if ($join->getTabela() != null && !isset($this->contadorJoin[$join->getTabela()])) {
            $this->contadorJoin[$join->getTabela()] = true;
            $this->join = $this->join === null ? [] : $this->join;
            array_push($this->join, $join);
        }
    }

    /**
     * @param String $tableName
     */
    function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    /**
     * @param Array $colunas
     */
    function setColunas($colunas)
    {
        $this->colunas = $colunas;
    }

    /**
     * Caminho completo da hydartor por name space
     * @param String $hydartor
     */
    function setHydartor($hydartor)
    {
        $this->hydartor = $hydartor;
    }

    /**
     * Condições enclausuradas, por setWhere
     * OBS:(Cada objeto do tipo Where sera colocado entre parentese para ser executado
     * separadamente de proximas condições adicionas por setWhere's sub-sequentes)
     * @param Where $where
     */
    function setWhere($where, $or = 'AND')
    {
        $this->where = $this->where === null ? [] : $this->where;
        array_push($this->where, [$where, $or]);
    }

    /**
     * Define tabela diferente ao longo de uma unica execução.
     * OBS: Tomar cuidado ao utilizar, pode alterar resultados requeridos
     * @param String $fromPersonalizado
     */
    function setFromPersonalisado($fromPersonalizado)
    {
        $this->from = $fromPersonalizado;
    }

    /**
     * Define se o select devera ter limit e offset
     * @param boolean $limitOffset
     */
    function setLimitOffset($limitOffset)
    {
        $this->limitOffset = $limitOffset;
    }

    function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;
    }

    /**
     * Monta select dinamicamente para auxiliar e agilizar a criação de query de grid,
     * e seus totalizadores
     * @param APIGridEntityAction $post
     * @return object
     */
    public function getResultadoDb(APIGridEntityAction $post)
    {

        $where = new Where();
        $selectCount = $this->getSelect()
            ->columns($this->getColunasTotalizador());

        $select = $this->getSelect()
            ->columns($this->getColunas());

        $class = $this->getHydartor();
        $coluna = $class::getColuna($post->getOrdenacao()) ?: $post->getOrdenacao();
        $colunaPesquisa = $class::getColuna($post->getColunaBusca()) ?: $post->getColunaBusca();

        if ($coluna) {
            $nextOrdenacao = ""; // ", id asc";
            $select->order($post->getNextOrdenacao() . $coluna . ' ' . $post->getDirecaoOrdenacao()
                . $nextOrdenacao);
        }

        $valorBusca = $post->getBusca();
        $tipoBusca = $post->getBuscaTipo();
        if ($valorBusca && $colunaPesquisa) {
            if ($tipoBusca == "like") {
                $colunaPesquisa = [$colunaPesquisa => new Expression("UPPER($colunaPesquisa)")];
                $valorBusca = strtoupper($valorBusca);
            } else if (count(explode(",", $valorBusca)) > 1) {
                $where->in([$colunaPesquisa => new Expression("UPPER($colunaPesquisa)")],
                    explode(",", strtoupper($valorBusca)));
            }

            if ($colunaPesquisa) {
                $where->{$tipoBusca}($colunaPesquisa, $valorBusca);
            }
        }

        if ($this->getFromPersonalisado() != null) {
            $select->from(new Expression($this->getFromPersonalisado()));
        }

        if ($this->getWhere() != null) {
            foreach ($this->getWhere() as $index => $valo) {
                $andOr = strtolower($valo[1]);
                if ($andOr == "or") {
                    $where->orPredicate($valo[0]);
                } else {
                    $where->addPredicate($valo[0]);
                }
            }
        }

        if ($this->getJoin() != null) {
            foreach ($this->getJoin() as $index => $valor) {
                $select->join($valor->getTabela(), $valor->getCondicao(), $valor->getColunas(), $valor->getForma());
                $selectCount->join($valor->getTabela(), $valor->getCondicao(), $valor->getColunas(), $valor->getForma());
            }
        }

        if ($this->getGroupBy() != null) {
            $select->group($this->getGroupBy());
            $selectCount->group($this->getGroupBy());
        }

        if ($this->getLimitOffset() != null) {
            $select->offset(intval($post->getOffset()))
                ->limit(intval($post->getLimit()));
        }

//        echo($select->where($where)
//                        ->getSqlString($this->getDbAdapter()->getPlatform()));die();

        $this->setSqlString($select->where($where)
                ->getSqlString($this->getDbAdapter()->getPlatform()));

        $this->getDbAdapter()->query('SET datestyle TO ISO, DMY;');

        $db = ConvertObject::convertObject($this->select($select->where($where)));
        $count = $this->select($selectCount->where($where))->count();

        return new APIGridReturnEntity($post->getDraw(), $count, $count, $db);
    }

    /**
     * Monta select dinamicamente para auxiliar e agilizar a criação de query de grid,
     * e seus totalizadores
     * @param APIGridEntityAction $post
     * @return object
     */
    public function getResultadoDbVendas(APIGridEntityAction $post)
    {

        $where = new Where();
        $selectCount = $this->getSelect()
            ->columns($this->getColunasTotalizador());

        $select = $this->getSelect()
            ->columns($this->getColunas());
        $class = $this->getHydartor();
        $coluna = $class::getColuna($post->getOrdenacao());
        $colunaBusca = $class::getColuna($post->getColunaBuscaTipo());

        $ordenacao = '';
        if ($coluna) {
            $ordenacao = $post->getNextOrdenacao() . $coluna . ' ' . $post->getDirecaoOrdenacao();
            $select->order($ordenacao);
        }

        if ($colunaBusca) {
            $tipoBusca = $post->getBuscaTipo();
            if ($post->getBusca() && is_numeric($post->getBusca())) {
                $where->{$tipoBusca}($colunaBusca, floatval($post->getBusca()));
            } elseif ($post->getBusca() && is_string($post->getBusca())) {
                if ($tipoBusca !== 'equalTo') {
                    $valorBusca = (!$tipoBusca) ?
                        '%' . strtoupper($post->getBusca()) . '%' :
                        strtoupper($post->getBusca()) . '%';

                    $where->like(
                        [$colunaBusca => new Expression("UPPER($colunaBusca)")], $valorBusca
                    );
                } elseif (count(explode(",", $post->getBusca())) > 1) {
                    $where->in($colunaBusca, explode(",", $post->getBusca()));
                } elseif (count(explode("%", $post->getBusca())) > 1) {
                    $where->like($colunaBusca, $post->getBusca());
                } else {
                    if ($colunaBusca !== "fatura" && count($post->getBusca()) == 1) {
                        if ($colunaBusca !== 'fatura' && $colunaBusca !== 'cfistaped') {
                            if ($post->getBusca() === "B") {
                                $post->setBusca(" ");
                            }
                            $where->equalTo($colunaBusca, $post->getBusca());
                        } else {
                            $where->equalTo(
                                [$colunaBusca => new Expression("UPPER($colunaBusca)")], $post->getBusca()
                            );
                        }
                    } else {
                        if ($post->getBusca() === 'F') {
                            $where->addPredicates(" MXPEDCFI.CFISTAPED IN ('Venda','PrazP')");
                        } elseif ($post->getBusca() === 'E') {
                            $where->addPredicates(" ( MXPEDCFI.CFIROTPED != 'A' AND MXPEDCFI.CFIROTPED != ' ' AND MXPEDCFI.CFITPNPED NOT LIKE '%NF%')");
                        } elseif ($post->getBusca() === 'A') {
                            $where->addPredicates(" ( MXPEDCFI.CFITPNPED NOT LIKE '%NF%' AND( MXPEDCFI.CFIROTPED = 'A' OR MXPEDCFI.CFIROTPED = ' '))");
                        }
                    }
                }
            }
        }

        if ($this->getFromPersonalisado() != null) {
            $select->from(new Expression($this->getFromPersonalisado()));
        }

        if ($this->getWhere() != null) {
            foreach ($this->getWhere() as $index => $valo) {
                $andOr = strtolower($valo[1]);
                if ($andOr == "or") {
                    $where->orPredicate($valo[0]);
                } else {
                    $where->addPredicate($valo[0]);
                }
            }
        }

        if ($this->getJoin() != null) {
            foreach ($this->getJoin() as $index => $valor) {
                $select->join($valor->getTabela(), $valor->getCondicao(), $valor->getColunas(), $valor->getForma());
                $selectCount->join($valor->getTabela(), $valor->getCondicao(), $valor->getColunas(), $valor->getForma());
            }
        }

        if ($this->getGroupBy() != null) {
            $select->group($this->getGroupBy());
            $selectCount->group($this->getGroupBy());
        }

        if ($this->getLimitOffset() != null) {
            $select->offset(intval($post->getOffset()))
                ->limit(intval($post->getLimit()));
        }

        $this->setSqlString($select->where($where)
                ->getSqlString($this->getDbAdapter()->getPlatform()));

        $colunasAgregacao = explode(
                "FROM", $selectCount->getSqlString($this->getDbAdapter()->getPlatform())
            )[0];
//        $selectTotal = $colunasAgregacao . " FROM ( " . explode("LIMIT",
//                        $this->getSqlString())[0] . " ) COMERCIAL";
//        var_dump($selectTotal);
//        die();
//        $classEntity = str_replace("Mapper\Hydrator", "Entity",
//                $this->getHydartor());

        return [
            'tabela' => ConvertObject::convertObject($this->select($select->where($where))),
            'totalizador' => [0 => []]
//            'totalizador' => [0 =>
//                $class::get()->hydrate(
//                        $this->getDbAdapter()->query($selectTotal,
//                                Adapter::QUERY_MODE_EXECUTE)->toArray()[0],
//                        $classEntity::get()
//                )->toArray()
//            ]
        ];
    }

}

class ConvertObject
{

    /**
     * Converte HydratingResultSet para array utilizando os tratamentos dos
     *  getters/setters
     * @param  HydratingResultSet $resultSet
     * @return array
     */
    public static function resultSetToArray(HydratingResultSet $resultSet, array $colunas = null)
    {
        $lista = [];
        foreach ($resultSet as $entity) {
            $row = $entity->toArray();
            if ($colunas) {
                $filter = function ($key) use ($colunas) {
                    return in_array($key, $colunas);
                };
                $row = array_filter($row, $filter, ARRAY_FILTER_USE_KEY);
            }
            $lista[] = $row;
        }
        return $lista;
    }

    public static function convertObject($obj)
    {
        $lista = [];
        foreach ($obj as $chave => $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key) ?: "");
                if (is_object($registros)) {
                    foreach ((array) $registros as $i => $val) {
                        $lista[$chave][$key][$i] = trim($val);
                    }
                } else {
                    $lista[$chave][$key] = trim($registros);
                }
            }
        }
        return $lista;
    }

    public static function convertObjectIncludeKeyCod($obj)
    {
        $lista = null;
        foreach ($obj as $chave => $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key));
                $lista[$value->getCodigo()][$key] = trim($registros);
            }
        }
        return $lista;
    }

    public static function convertEntity($obj)
    {
        $lista = [];
        foreach ((array) $obj as $key => $value) {
            $key = ltrim(str_replace("*", "", $key));
            $lista[$key] = is_string($value) ? trim($value) : $value;
        }
        return $lista;
    }

    public static function convertObjectEmUmaChave($obj)
    {
        $lista = null;
        foreach ($obj as $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key));
                $lista[$key] = trim($registros);
            }
        }
        return $lista;
    }

}
