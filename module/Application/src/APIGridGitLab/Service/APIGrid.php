<?php

namespace APIGrid\Service;

use Zend\Stdlib\Parameters;
use APIFiltro\Entity\Filtros;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class APIGrid extends Helper
{

    public function getPostTratado(Parameters $post)
    {
        $postTratado = new APIGridEntityAction();
        $filtroAuxilio = new Filtros();
        $filtroAuxilio->exchangeArray($post->get('filtros', []));

        $postTratado->setOffset($post->get('start', 0));
        $postTratado->setLimit($post->get('length', 10));

        $order = $post->get('order')[0];
        $postTratado->setDirecaoOrdenacao($order['dir']);
        $postTratado->setOrdenacao($post->get('columns')[$order['column']]['data']);

        $postTratado->setFormaBusca($filtroAuxilio->getFormaBusca());
        $postTratado->setColunaBusca($filtroAuxilio->getColunaBusca());

        $valorBusca = $post->get('search', ["value" => ""])['value'];

        $tipoBusca = $filtroAuxilio->getFormaBusca();
        if ($tipoBusca == '0' || $tipoBusca == '1') {
            $tipoBusca = 'like';
            if ($postTratado->getFormaBusca()) {
                $valorBusca = "$valorBusca%";
            } else {
                $valorBusca = "%$valorBusca%";
            }
        }

        $postTratado->setBusca($valorBusca);
        $postTratado->setBuscaTipo($tipoBusca);

        return $postTratado;
    }

}
