<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Usuarios\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractRestfulController
{

    public function tarefasAction()
    {
        $login = new ViewModel();
        $login->setTemplate('tarefas');
        return $login;
    }

    public function contaAction()
    {
        $login = new ViewModel();
        $login->setTemplate('conta');
        return $login;
    }

}
