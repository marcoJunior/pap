<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Bi\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractRestfulController
{

    public function clientesAction()
    {
        $login = new ViewModel();
        $login->setTemplate('bi/clientes/index');
        return $login;
    }
    
    public function fornecedoresAction()
    {
        $login = new ViewModel();
        $login->setTemplate('bi/fornecedores/index');
        return $login;
    }
    
    public function produtosAction()
    {
        $login = new ViewModel();
        $login->setTemplate('bi/produtos/index');
        return $login;
    }

}
